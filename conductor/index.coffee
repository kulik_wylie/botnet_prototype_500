

require './globals'
c '\n'
c color.blue "conductor 500", off
c '\n'


# NOTE In general, the root env would be coming out of a Redis store cache
# in order that the system can maintain some state over code refresh reloads


# { dev } = require '../configs'


config = (require '../configs')["#{process.argv[process.argv.length - 1]}"]


# root_env = Imm.Map
#     brujo_socket_port: 8999
#     conductor_server_port: 9003


( require './helix' ) { env : config }
