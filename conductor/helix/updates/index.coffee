

api = {}


# api = fp.assign api, require('./init').default
# api = fp.assign api, require('./base_primus').default
# api = fp.assign api, require('./res_spawn').default


keys_api = _.keys api


updates = ({ effects }) ->

    ({ state, action }) ->
        if _.includes(keys_api, action.type)
            api[action.type] { state, action, effects }
        else
            unless action.type is 'heartbeat'
                c "reducer no-op, #{action.type}"
            state


exports.default = updates
