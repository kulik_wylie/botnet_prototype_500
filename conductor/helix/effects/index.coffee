

api = {}


api = fp.assign api, require('./init_primus').default
api = fp.assign api, require('./spawn_player_bots').default

keys_api = _.keys api


effects_precursor = ({ Dispatch, env }) ->
    dispatch = (opts) ->
        Dispatch.emit 'new_action', { action, opts }

    ({ type, payload }) ->
        if (_.includes keys_api, type )
            api[type] { payload, dispatch }
        else
            c "No-op in effects api with type", type


exports.default = effects_precursor
