

require './globals'
c '\n'
c color.purple "bot 500", off
c '\n'

# NOTE In general, the root env would be coming out of a Redis store cache
# in order that the system can maintain some state over code refresh reloads




# { dev } = require '../configs'




config = (require '../configs')["#{process.argv[process.argv.length - 1]}"]





# root_env = Imm.Map
#     brujo_socket_port: 8999
#     conductor_server_port: 9003


( require './helix' ) { env : config }







# We actually don't need a specialised cohort_leader program, because that can just be another type of bot.
# from cohort_leader.coffee:
# While in development, bots can be processes in the same environment as the conductor, in production and at scale there may be very many bots, and these will be typically deployed to other VMs on the cloud.
# Under such a scenario, there will typically be a variety of bots on a given machine, and to organise these we have this program to organise this.
# This should probably be a Docker container eventually.
