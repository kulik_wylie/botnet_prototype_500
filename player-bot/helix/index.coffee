



EventEmitter = require 'events'
class Emitter extends EventEmitter
process.setMaxListeners 10000


module.exports = Bluebird.promisify ({ env }) ->
    initial_state = require('./initial_state').default { env }
    Dispatch = new Emitter()
    effects = require('./effects').default { Dispatch, env }
    updates = require('./updates').default { effects }

    Dispatch.on 'new_action', ({ action }) ->
        state = updates { state, action }
