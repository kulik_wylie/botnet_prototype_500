

{
    applyMiddleware, compose, createStore
} = require 'redux'
{ combineReducers } = require 'redux-immutable'
thunk = require('redux-thunk').default


initial_state = require './initial_state'


exports.default = ({ effects }) ->
    lookup = require('./reducers/lookup.coffee').default { effects }
    reducers = { lookup } # reducers =, and returned
    createStore(combineReducers(reducers), initial_state, applyMiddleware(thunk))
