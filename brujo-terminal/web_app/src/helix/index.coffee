

EventEmitter = require 'events'
class Emitter extends EventEmitter
Dispatch = new Emitter()





effects = require('./effects.coffee').default { Dispatch }


store = require('./state/create.coffee').default { effects }


Dispatch.on 'new_action', ({ action }) ->
    store.dispatch action


exports.store = store


# exports.effects = effects
# we could export effects, but my opinion is currently to keep the
