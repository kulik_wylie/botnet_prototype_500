


# Architecture



At first, the bots are instantiated simply as separet processes in the same environment, but we'll be building them with an eye to deploying bots to other environments, VPNs on the cloud somewhere.  


## Components:


### conductor :
controls the player-bots
### player-bot :
performs interactions to simulate real-world system elements such as subscribers, and other elements.  Most of the bots will play various parts of the world system we are simulating, but there will be some utility bots that organise the lifecycle of other bots. For example, we may have dedicated VMs hosting a variety of bot processes remote from the Conductor process environment VM.  The management of a particular VM environment may be handled by a particular utility bot, controlled from the same conductor.
### message-queue :
data from the bots and the tested-system's aggregate on the message-queue. RabbitMQ.
### mq-interpreter :
reduces and interprets the message-queue, in order to provide a useful data stream to the
### brujo-terminal :
provides control surface to the conductor, as well as instrumentation from the mq-interpreter.  Implementing initial version of this as a web-app in ReactJS with an Express asset server.  It will open a websocket connection to the Conductor component for control surface, and a websocket connection to the Mq-interpreter for instrumentation surface (where 'control' means the active/agency and 'instrumentation' means the passive display of information).














_________________________________________




##### Starting rabbitmq server locally:  `/usr/local/Cellar/rabbitmq/3.7.7/sbin/rabbitmq-server`



____________________________


*Benevolent Botnets*
